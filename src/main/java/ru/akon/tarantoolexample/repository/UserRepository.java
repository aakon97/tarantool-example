package ru.akon.tarantoolexample.repository;

import org.springframework.data.repository.CrudRepository;
import ru.akon.tarantoolexample.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
}