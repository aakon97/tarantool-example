package ru.akon.tarantoolexample.model;

import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.tarantool.core.mapping.Field;
import org.springframework.data.tarantool.core.mapping.Tuple;

@Tuple("user")
@Getter
@Setter
public class User  {
    @Id
    @Field("user_id")
    String id;
    String name;
    Integer age;
    @Field("sex")
    Boolean gender;
}
