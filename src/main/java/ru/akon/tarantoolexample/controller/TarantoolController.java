package ru.akon.tarantoolexample.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.akon.tarantoolexample.service.TarantoolService;

@RequiredArgsConstructor
@RestController
@RequestMapping("jooq")
@Tag(description = "jooq", name = "jooq")
public class TarantoolController {

    private final TarantoolService tarantoolService;

    @Operation(description = "Выборка")
    @PostMapping("select")
    public void select() {
        tarantoolService.select();
    }

}
