package ru.akon.tarantoolexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class TarantoolExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TarantoolExampleApplication.class, args);
	}

}
