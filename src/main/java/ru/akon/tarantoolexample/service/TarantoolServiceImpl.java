package ru.akon.tarantoolexample.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.akon.tarantoolexample.model.User;
import ru.akon.tarantoolexample.repository.UserRepository;


@Slf4j
@Service
@RequiredArgsConstructor
public class TarantoolServiceImpl implements TarantoolService {

    private final UserRepository repository;

    public void select() {
        User user = new User();
        user.setName("Alexey Petrov");
        user.setAge(28);
        user.setGender(true);
        User saved = repository.save(user);
        Iterable<User> userList = repository.findAll();

        for (User user1 : userList) {
            log.info(user1.getId());
        }

        log.info("123");
    }

}
